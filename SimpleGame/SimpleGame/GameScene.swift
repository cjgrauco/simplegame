//
//  GameScene.swift
//  SimpleGame
//
//  Created by Johan Graucob on 2015-08-26.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    // private constant for the player, example of a sprite
    let player = SKSpriteNode(imageNamed: "player")
    
    override func didMoveToView(view: SKView) {
        // background set to white
        backgroundColor = SKColor.whiteColor()
        
        // position of the sprite, 10% across verticall and centered horizontally
        player.position = CGPoint(x: size.width * 0.1, y: size.height * 0.5)
        
        // add as child to the scene do make it appear.
        addChild(player)
    }
}