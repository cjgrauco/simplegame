//
//  GameScene.swift
//  SimpleGame
//
//  Created by Johan Graucob on 2015-08-26.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

//bit mask for hit detection categories, singel 32 bit int, each bit is a category
struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let Monster   : UInt32 = 0b1       // 1 first bit represents the monster
    static let Projectile: UInt32 = 0b10      // 2 second bit the projectile
}

import SpriteKit


//vector math functions from http://www.raywenderlich.com/84434/sprite-kit-swift-tutorial-beginners
func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
    func sqrt(a: CGFloat) -> CGFloat {
    return CGFloat(sqrtf(Float(a)))
    }
#endif

extension CGPoint {
    func length() -> CGFloat {
        return sqrt(x*x + y*y)
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}

class GameScene: SKScene, SKPhysicsContactDelegate {
   
    // private constant for the player, example of a sprite
    let player = SKSpriteNode(imageNamed: "player")
    
    
    func random() -> CGFloat{
        
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random (#min: CGFloat, max: CGFloat) ->CGFloat{
        return random() * (max - min) + min
    }
    
    
    func addMonster(){
        
        //create sprite
        let monster = SKSpriteNode(imageNamed: "monster")
        
        monster.physicsBody = SKPhysicsBody(rectangleOfSize: monster.size) // 1 Creates body for sprite
        monster.physicsBody?.dynamic = true // 2 sets it to be dynamic, the phys engine does not control the object
        monster.physicsBody?.categoryBitMask = PhysicsCategory.Monster // 3 sets the bit mask to monster
        monster.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile // 4 when should the delegate be notified
        monster.physicsBody?.collisionBitMask = PhysicsCategory.None // 5 what bitmasks should the monster bounce off of
        
        // spawn location along Y axis
        let actualY = random(min: monster.size.height/2, max: size.height - monster.size.height/2)
        
        //position monster slightly off screen, and along a random position along the Y axis
        monster.position = CGPoint(x: size.width + monster.size.width/2, y: actualY)
        
        addChild(monster)
        
        // speed of monster
        let actualDuration = random(min: CGFloat(2.0), max:CGFloat(4.0))
        
        
        // perform actions
        
        // MoveTo directs the monster to move off screen to the left, speed between 2-4 seconds.
        let actionMove = SKAction.moveTo(CGPoint(x: -monster.size.width/2, y:actualY), duration: NSTimeInterval(actualDuration))
        
        //removeFromParent deletes the monster from the game
        let actionMoveDone = SKAction.removeFromParent()
        
        //runAction chains together the 2 actions
        monster.runAction(SKAction.sequence([actionMove, actionMoveDone]))
        
    }
    
    
    override func didMoveToView(view: SKView) {
        
        //sets the world to have no gravity
        physicsWorld.gravity = CGVectorMake(0,0)
        
        //sets the scene to be notified when two bodies collide
        physicsWorld.contactDelegate = self
        
        
        // background set to white
        backgroundColor = SKColor.whiteColor()
        
        // position of the sprite, 10% across verticall and centered horizontally
        player.position = CGPoint(x: size.width * 0.1, y: size.height * 0.5)
        
        // add as child to the scene do make it appear.
        addChild(player)
        
        // adds infinte monsters with a 2 second wait time
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addMonster),SKAction.waitForDuration(1)])
            ))
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        // 1
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // 2
        if ((firstBody.categoryBitMask & PhysicsCategory.Monster != 0) &&
            (secondBody.categoryBitMask & PhysicsCategory.Projectile != 0)) {
                projectileDidCollideWithMonster(firstBody.node as SKSpriteNode, monster: secondBody.node as SKSpriteNode)
        }
        
    }
    
    func projectileDidCollideWithMonster(projectile:SKSpriteNode, monster:SKSpriteNode) {
        println("Hit")
        projectile.removeFromParent()
        monster.removeFromParent()
    }
    
    
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        
        // 1 - Choose one of the touches to work with
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self) // sets the coordinates of the touch
        
        // 2 - Set up initial location of projectile
        
        //creates a new SpriteNode projectile
        let projectile = SKSpriteNode(imageNamed: "projectile")
        
        // sets physics, collision
        projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/2)
        projectile.physicsBody?.dynamic = true
        projectile.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
        projectile.physicsBody?.contactTestBitMask = PhysicsCategory.Monster
        projectile.physicsBody?.collisionBitMask = PhysicsCategory.None
        projectile.physicsBody?.usesPreciseCollisionDetection = true
        
        //sets the position to be the same as the player
        projectile.position = player.position
        
        // 3 - Determine offset of location to projectile
        
        // gets a vector form the current position to the touch location
        let offset = touchLocation - projectile.position
        
        // 4 - Bail out if you are shooting down or backwards
        if (offset.x < 0) { return }
        
        // 5 - OK to add now - you've double checked position
        addChild(projectile)
        
        // 6 - Get the direction of where to shoot
        
        //converts the offset into a unit vector of length one
        let direction = offset.normalized()
        
        // 7 - Make it shoot far enough to be guaranteed off screen
        let shootAmount = direction * 1000
        
        // 8 - Add the shoot amount to the current position
        let realDest = shootAmount + projectile.position
        
        // 9 - Create the actions
        
        //creates the action
        let actionMove = SKAction.moveTo(realDest, duration: 2.0)
        //removes the action from the scene
        let actionMoveDone = SKAction.removeFromParent()
        //executes the action of first shooting the node then deleting the node
        projectile.runAction(SKAction.sequence([actionMove, actionMoveDone]))
        
    }
}